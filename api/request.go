package api

type LoginRegister struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Code     string `json:"code"`
}

type UserProfile struct {
	Phone     string `json:"phone"`
	Address   string `json:"address"`
	Full_name string `json:"full_name"`
}

type GoogleSignup struct {
	Code string `json:"code"`
}
