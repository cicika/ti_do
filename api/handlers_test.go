package api

import (
	_ "ti_do/data"
	"ti_do/db"

	"bytes"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Authentication(t *testing.T) {
	address, md := startTestServer()
	md.On("SelectApiAccessToken", "").Return(0, errors.New("error"))

	res, err := executeRequest("GET", nil, address, "/api/user/asdf")

	if err != nil {
		fmt.Println(err.Error())
	}

	assert.Equal(t, res.StatusCode, http.StatusForbidden)
}

func startTestServer() (string, *db.MySqlConnMock) {
	md := &db.MySqlConnMock{}
	addr := fmt.Sprintf(
		"%s:%d", "localhost", port())
	api := &ApiServer{
		db:      md,
		address: addr,
	}

	go api.startApiServer()

	return addr, md
}

// generates a random, five digit port
func port() *big.Int {
	min := big.NewInt(10000)
	max := big.NewInt(int64(65536 - 10000))
	p, err := rand.Int(rand.Reader, max)
	if err != nil {
		return port()
	} else {
		return p.Add(p, min)
	}
}

func executeRequest(method string, req interface{}, address, url string) (*http.Response, error) {
	cl := &http.Client{}
	var body io.Reader
	if req != nil {
		j, _ := json.Marshal(&req)
		body = bytes.NewBuffer(j)
	}

	hreq, _ := http.NewRequest(method, fmt.Sprintf("http://%s/%s", address, url), body)
	res, err := cl.Do(hreq)
	return res, err
}
