package api

import (
	"ti_do/data"
	"ti_do/db"
	svc "ti_do/services"

	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type ApiServer struct {
	db      db.MysqlConnectionI
	address string
	server  *http.Server
	google  svc.GoogleClientI
	email   svc.EmailSenderI
}

func NewApiServer(db db.MysqlConnectionI) {
	adr := fmt.Sprintf(
		"%s:%d",
		data.ApiConfig.ApiHost, data.ApiConfig.ApiPort)

	as := &ApiServer{
		db:      db,
		address: adr,
		google:  svc.NewGoogleClient(),
		email:   svc.NewEmailSender(),
	}
	as.startApiServer()
}

func (as *ApiServer) startApiServer() {
	router := mux.NewRouter().StrictSlash(true)
	freeRouter := router.PathPrefix("/api").Subrouter()
	authRouter := router.PathPrefix("/api").Subrouter()

	authRouter.Use(as.authentication)

	freeRouter.Methods("POST").Path("/google-signup").HandlerFunc(as.GoogleSignup)
	freeRouter.Methods("POST").Path("/login").HandlerFunc(as.Login)
	freeRouter.Methods("POST").Path("/registration").HandlerFunc(as.RegisterUser)
	freeRouter.Methods("GET").Path("/verify-email/{code}").HandlerFunc(as.VerifyEmail)
	freeRouter.Methods("POST").Path("/forgot-password").HandlerFunc(as.NewPasswordCode)
	freeRouter.Methods("POST").Path("/reset-password").HandlerFunc(as.ResetPassword)
	authRouter.Methods("GET").Path("/user/profile").HandlerFunc(as.GetUser)
	authRouter.Methods("PUT").Path("/user/profile").HandlerFunc(as.PutUserProfile)
	authRouter.Methods("GET").Path("/logout").HandlerFunc(as.Logout)

	srv := &http.Server{
		Addr:    as.address,
		Handler: router,
	}
	as.server = srv

	err := srv.ListenAndServe()
	if err != nil {
		log.Fatal("REST API failed to start: ", err.Error())
	}
}

func (as *ApiServer) Shutdown(ctx context.Context) {
	as.server.Shutdown(ctx)
}
