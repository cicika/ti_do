package api

import (
	"crypto/md5"
	"fmt"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func GenerateUuid() (string, error) {
	uUid, err := uuid.NewUUID()
	if err != nil {
		return "", err
	}
	return uUid.String(), nil
}

func GenerateCode(email, uuid string) string {
	sum := md5.Sum([]byte(email + uuid))

	return fmt.Sprintf("%x", sum)
}

func GenerateApiAccessToken() string {
	u, _ := uuid.NewUUID()
	token := md5.Sum([]byte(u.String()))

	return fmt.Sprintf("%x", token)
}
