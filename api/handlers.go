package api

import (
	"ti_do/db"

	"database/sql"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/context"
	"github.com/gorilla/mux"
)

func (as *ApiServer) Login(w http.ResponseWriter, r *http.Request) {
	var data LoginRegister
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &data)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	user, err := as.db.Login(data.Email)

	if user.Id == 0 && user.Uuid == "" {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !CheckPasswordHash(data.Password, user.Password) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	newAccessToken := GenerateApiAccessToken()
	as.db.UpdateApiAccessToken(user.Id, newAccessToken)

	u, up, _, err := as.db.SelectUser(user.Id, user.Uuid, "")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	result := FromModel(u, up, newAccessToken, true)
	j, _ := json.Marshal(result)

	w.Write(j)
}

func (as *ApiServer) GoogleSignup(w http.ResponseWriter, r *http.Request) {
	var g GoogleSignup
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &g)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	info, err := as.google.GetProfileInfo(g.Code)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	uuid, err := GenerateUuid()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	user := db.User{
		Uuid:          uuid,
		Email:         info["email"].(string),
		EmailVerified: info["email_verified"].(bool),
		GoogleAccount: true,
	}

	apiAccessToken := GenerateApiAccessToken()
	userId, err := as.db.InsertUser(&user)
	if err != nil {
		if !strings.Contains(err.Error(), "Error 1062") {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			// user exists, logging in...
			user, uProfile, _, err := as.db.SelectUser(-1, "", info["email"].(string))

			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			as.db.UpdateApiAccessToken(user.Id, apiAccessToken)
			result := FromModel(user, uProfile, apiAccessToken, true)
			j, _ := json.Marshal(result)
			w.Write(j)
			return
		}
	}

	userProfile := db.UserProfile{
		UserId:   userId,
		UserUuid: user.Uuid,
		FullName: sql.NullString{Valid: true, String: info["name"].(string)},
	}
	as.db.InsertUserProfile(&userProfile)
	as.db.InsertApiAccessToken(userId, apiAccessToken)

	full := FullUserProfile{
		Uuid:          user.Uuid,
		Email:         user.Email,
		Fullname:      info["name"].(string),
		AccessToken:   apiAccessToken,
		GoogleAccount: true,
		EmailVerified: user.EmailVerified,
		Existing:      false,
	}
	j, _ := json.Marshal(full)

	w.Write(j)
}

func (as *ApiServer) RegisterUser(w http.ResponseWriter, r *http.Request) {
	var data LoginRegister
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &data)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	hashed, _ := HashPassword(data.Password)
	uuid, err := GenerateUuid()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	apiAccessToken := GenerateApiAccessToken()
	emailCode := GenerateCode(data.Email, uuid)
	go as.email.SendVerificationEmail(data.Email, emailCode)

	user := &db.User{
		Email:                 data.Email,
		Uuid:                  uuid,
		Password:              hashed,
		EmailVerificationCode: sql.NullString{Valid: true, String: emailCode},
	}

	userId, err := as.db.InsertUser(user)
	if err != nil {
		if !strings.Contains(err.Error(), "Error 1062") {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else {
			// user exists
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
	userProfile := &db.UserProfile{
		UserId:   userId,
		UserUuid: uuid,
	}

	log.Println(userProfile)
	as.db.InsertUserProfile(userProfile)
	as.db.InsertApiAccessToken(userId, apiAccessToken)

	result := FromModel(user, userProfile, apiAccessToken, false)
	j, _ := json.Marshal(&result)

	w.Write(j)
}

func (as *ApiServer) PutUserProfile(w http.ResponseWriter, r *http.Request) {
	var up UserProfile
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &up)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	userId := context.Get(r, "userId").(int64)
	var phone sql.NullString
	var address sql.NullString
	var fullname sql.NullString
	if up.Phone != "" {
		phone = sql.NullString{Valid: true, String: up.Phone}
	}
	if up.Address != "" {
		address = sql.NullString{Valid: true, String: up.Address}
	}
	if up.Full_name != "" {
		fullname = sql.NullString{Valid: true, String: up.Full_name}
	}
	userP := &db.UserProfile{
		UserId:   userId,
		Phone:    phone,
		Address:  address,
		FullName: fullname,
	}
	_, err = as.db.UpdateUserProfile(userP)
	if err != nil {
		log.Println(err.Error())
	}

	user, uProfile, token, err := as.db.SelectUser(userId, "", "")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	result := FromModel(user, uProfile, token, true)
	j, _ := json.Marshal(result)

	w.Write(j)
}

func (as *ApiServer) GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userUuid := params["uuid"]
	userId := context.Get(r, "userId").(int64)

	u, up, token, err := as.db.SelectUser(userId, userUuid, "")
	if u.Id == 0 || err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	result := FromModel(u, up, token, true)

	j, _ := json.Marshal(&result)

	w.Write(j)
}

func (as *ApiServer) VerifyEmail(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	code, ok := vars["code"]
	if ok {
		email, err := as.db.CheckEmailVerificationCode(code)
		if email == "" || err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		as.db.VerifyEmail(email)
		w.WriteHeader(http.StatusOK)
		return
	}
	w.WriteHeader(http.StatusBadRequest)
}

func (as *ApiServer) NewPasswordCode(w http.ResponseWriter, r *http.Request) {
	var data LoginRegister
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &data)

	if err != nil || data.Email == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	passwordCode := GenerateCode(data.Email, "")

	_, err = as.db.InsertPasswordCode(data.Email, passwordCode)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	go as.email.SendResetPasswordEmail(data.Email, passwordCode)

	w.WriteHeader(http.StatusOK)
}

func (as *ApiServer) ResetPassword(w http.ResponseWriter, r *http.Request) {
	var data LoginRegister
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &data)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	hashed, _ := HashPassword(data.Password)
	err = as.db.UpdatePassword(hashed, data.Code)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (as *ApiServer) Logout(w http.ResponseWriter, r *http.Request) {
	userId := context.Get(r, "userId").(int64)

	err := as.db.InvalidateAccessToken(userId)
	if err != nil {
		log.Println(err.Error())
	}
	w.WriteHeader(http.StatusOK)
}
