package api

import (
	"ti_do/db"
)

type FullUserProfile struct {
	Uuid              string `json:"user_uuid"`
	Email             string `json:"email"`
	Phone             string `json:"phone"`
	Fullname          string `json:"full_name"`
	Address           string `json:"address"`
	AccessToken       string `json:"access_token"`
	GoogleAccount     bool   `json:"google_account"`
	EmailVerified     bool   `json:"email_verified"`
	ResetPasswordCode string `json:"code"`
	EmailVeificationCode string 
	Existing          bool   `json:"existing_user"`
}

func FromModel(
	u *db.User, up *db.UserProfile, token string, existing bool) *FullUserProfile {
	return &FullUserProfile{
		Uuid:          u.Uuid,
		Email:         u.Email,
		Phone:         up.Phone.String,
		Fullname:      up.FullName.String,
		Address:       up.Address.String,
		AccessToken:   token,
		GoogleAccount: u.GoogleAccount,
		EmailVerified: u.EmailVerified,
		Existing:      existing,
	}
}
