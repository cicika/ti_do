package api

import (
	"net/http"

	"github.com/gorilla/context"
)

func (as *ApiServer) authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accessToken := r.Header.Get("X-Api-Token")
		userId, err := as.db.SelectApiAccessToken(accessToken)

		if userId == -1 || err != nil {
			http.Error(w, "Forbidden", http.StatusForbidden)
		} else {
			context.Set(r, "userId", userId)
			next.ServeHTTP(w, r)
		}
	})
}
