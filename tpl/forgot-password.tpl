{{ define "forgot-password" }}
{{ template "header" }}
<section>
 {{ template "user-message" }}
</section>
<section>
  <h3>Forgotten password</h3>
  <p>An email will be sent to provided address.<br />
  Please follow the instructions upon receiving it.</p>
  <form name="forgot-password" method="POST" action="/forgot-password">
    <div>
    <label for="email">Email </label>
    <input type="text" name="email" />
    </div>
    <input type="submit" value="Send" />
  </form>
</section>
<section>
<div>
 <a href="/">Back to Login</a>
</div>
</section>
{{ template "footer" }}
{{ end }}
