{{ define "profile" }}
{{ template "header" }}
<section>
 {{ template "user-message" }}
</section>
<section>
  <h3>Hello!</h3>
  <p>Email: {{ .User.Email }}</p>
  <p>Full Name: {{ .User.Fullname }}</p>
  <p>Address: {{ .User.Address }}</p>
  <p>Phone: {{ .User.Phone }}</p>
  <a href="/edit-profile">Edit profile</a>
</section>
<section>
<div>
<form name="logout" method="POST" action="/logout">
  <input type="hidden" value="" />
  <input type="submit" value="Logout" />
</form>
</div>
</section>
{{ template "footer" }}
{{ end }}