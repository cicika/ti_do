{{ define "header" }}
<!DOCTYPE html>
<html>
  <head>
    <title>{{ .PageTitle }}</title>
    <link href="./fe/style.css" relation="style" />
    <script src="./fe/functions.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js">
  </script>
  <script src="https://apis.google.com/js/client:platform.js?onload=start" async defer>
  </script>
  <script>
    function start() {
      gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
          client_id: '523105438593-vll7db4g5nf0jse689e1lps5otvejgji.apps.googleusercontent.com',
          // Scopes to request in addition to 'profile' and 'email'
          //scope: 'additional_scope'
        });
      });
    }
  </script>
  </head>
  <body>
{{ end }}