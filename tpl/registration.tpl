{{ define "registration" }}
{{ template "header" }}
<section>
 {{ template "user-message" }}
</section>
<section>
  <h3>Registration</h3>
  <p>All fields are required!</p>
  <form name="register" method="POST" action="/registration">
  <div>
    <label for="email">Email </label>
    <input type="text" name="email" />
  </div>
  <div>  
    <label for="password">Password: </label>
    <input type="password" size="25" name="password" />
  </div>  
  <div>
    <label for="password-again">Repeat password: </label>
    <input type="password" size="25" name="password-again" />
  </div> 
  <div> 
    <input type="submit" value="Send" />
  </div>
  </form>
</section>
<section>
 <a href="/">Back to Login</a>
</section>
{{ template "footer" }}
{{ end }}
