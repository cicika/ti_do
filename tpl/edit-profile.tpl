{{ define "edit-profile" }}
{{ template "header" }}
<section>
 {{ template "user-message" }}
</section>
<section>
  <h3>Edit profile</h3>
  <p></p>
  <form name="edit-profile" method="POST" action="/profile">
  <div>
    <label for="email">Email </label>
    <input type="text" name="email" value="{{.User.Email}}" disabled="{{.User.GoogleAccount}}"/>
  </div>
  <div>  
    <label for="full_name">Name: </label>
    <input type="text" size="65" name="full_name" value="{{.User.Fullname}}" />
   </div> 
   <div> 
    <label for="address">Address: </label>
    <input type="text" size="65" name="address" value="{{.User.Address}}" />
   </div> 
    <label for="phone">Phone number: </label>
    <input type="text" size="25" name="phone" value="{{.User.Phone}}" />
   </div>
  <div> 
    <input type="submit" value="Update profile" />
  </div>
  </form>
</section>
<section>
 <a href="/profile">Cancel</a>
</section>
{{ template "footer" }}
{{ end }}
