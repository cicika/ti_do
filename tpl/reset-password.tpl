{{ define "reset-password" }}
  {{ template "header" }}
    <section>
      {{ template "user-message" }}
    </section>
    <section>
      <h3>Reset password</h3>
      <p>You can now choose a new password</p>
      <form name="reset-password" method="POST" action="/reset-password">
      <div>
        <label for="password">Password: </label>
        <input type="password" size="25" name="password" />
      </div>
      <div>  
        <label for="password-again">Repeat password: </label>
        <input type="password" size="25" name="password-again" />
      </div>  
        <input type="hidden" name="code" value="{{.User.ResetPasswordCode}}" />
      <div>  
        <input type="submit" value="Reset password" />
      </div>
      </form>
    </section>
    <section>
    <div>
      <a href="/">Back to Login</a>
    </div>
    </section>
  {{ template "footer" }}
{{ end }}
