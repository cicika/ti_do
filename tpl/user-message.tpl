{{ define "user-message" }}
  {{ if .Error }}
    <p class="error"> {{ .Message }} </p>
  {{ end }}
  {{ if .Info }}
    <p class="info"> {{ .Message }} </p>
  {{ end }}
{{ end }}