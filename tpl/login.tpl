{{ define "login" }}
{{ template "header" }}
<section>
 {{ template "user-message" }}
</section>
<section>
  <form name="login" method="POST" action="/login">
    <div>
    <label for="email">Email </label>
    <input type="text" name="email" />
    </div>
    <div>
    <label for="password">Password </label>
    <input type="password" name="password" />
    </div>
    <div>
    <input type="submit" value="Login" />
    </div>
  </form>
</section>
<section>
<div>
 <a href="/registration">Sign Up</a> ::: <a href="/forgot-password">Forgot password</a>
 </div>
</section>
<section>
<div>
<button id="signinButton">Sign in with Google</button>
</div>
  <script>
    $('#signinButton').click(function() {
      auth2.grantOfflineAccess().then(signInCallback);
    });
  </script>
  <div style="display: none">
    <form name="google_login" id="google_login" method="POST" action="/google-signup">
      <input type="text" name="code" value="" id="google_code" />
      <input type="submit" value="Submit" />
    </form>
</section>
{{ template "footer" }}
{{ end }}
