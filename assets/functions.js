function signInCallback(authResult) {
  if (authResult['code']) {
    console.log(authResult);
    // Hide the sign-in button now that the user is authorized, for example:
    document.getElementById('signinButton').setAttribute('style', 'display: none')
    document.getElementById('google_code').setAttribute('value', authResult['code'])
    document.getElementById('google_login').submit()
  } else {
    // There was an error.
  }
}
