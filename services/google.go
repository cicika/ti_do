package services

import (
	"ti_do/data"

	"encoding/json"
	"io/ioutil"
	"log"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var conf *oauth2.Config

type GoogleClientI interface {
	GetProfileInfo(string) (map[string]interface{}, error)
}

type GoogleClient struct {
	config *oauth2.Config
}

func NewGoogleClient() *GoogleClient {
	var googleCreds *data.GoogleCredentials
	f, err := ioutil.ReadFile("./resources/creds.json")
	if err != nil {
		log.Println("Error opening google credentials file: ", err.Error())
	}
	err = json.Unmarshal(f, &googleCreds)
	if err != nil {
		log.Println("Error unmarshalling google credentials: ", err.Error())
		return &GoogleClient{}
	}
	conf = &oauth2.Config{
		ClientID:     googleCreds.ClientId,
		ClientSecret: googleCreds.ClientSecret,
		RedirectURL:  "http://cicika.ddnss.de:8081",
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
		},
		Endpoint: google.Endpoint,
	}
	return &GoogleClient{config: conf}
}

func (gc *GoogleClient) GetProfileInfo(code string) (map[string]interface{}, error) {
	var d map[string]interface{}

	token, err := gc.config.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.Println("Err with code: ", err.Error())
		return d, err
	}
	client := conf.Client(oauth2.NoContext, token)
	userinfo, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")

	if err != nil {
		log.Println(err.Error())
		return d, err
	}
	body, _ := ioutil.ReadAll(userinfo.Body)
	json.Unmarshal(body, &d)

	return d, nil
}
