package services

import (
	"ti_do/data"

	"fmt"
	"log"
	"net/smtp"
)

type EmailSenderI interface {
	SendVerificationEmail(string, string) error
	SendResetPasswordEmail(string, string) error
}

const (
	verifyEmailSubject   = "Please confirm your email address"
	resetPasswordSubject = "Password change requested"
	from                 = "no-reply@cicika.ddnss.de"
)

var verifyEmailText string = "To: %s\n\r" +
	"Hello, \n\r\n\r" +
	"In order to proceed with using our services, \n\r" +
	"we need you to validate your email address by clicking the link bellow: \n\r" +
	"<a href='http://cicika.ddnss.de:8081/verify-email/%s'>Email verification</a>\n\r"

// email, full frontend address, verification code

var resetPasswordText string = "To: %s\n\r" +
	"Hello, \n\r\n\r" +
	"The account with your email has requested a password reset.\n\r" +
	"If you requested this, please follow the link bellow in order\n\r" +
	"to update your password: \n\r" +
	"<a href='http://cicika.ddnss.de:8081/reset-password/%s'>Reset password</a>\n\r\n\r" +
	"If you did not request this change, please ignore this email."

type EmailSender struct {
	server string
}

func NewEmailSender() *EmailSender {
	return &EmailSender{
		server: data.ApiConfig.EmailServer,
	}
}

func (es *EmailSender) SendVerificationEmail(to, code string) error {
	msg := fmt.Sprintf(verifyEmailText, to, code)
	log.Println(msg)
	err := smtp.SendMail(es.server, nil, from, []string{to}, []byte(msg))
	if err != nil {
		log.Println(err.Error())
	}
	return err
}

func (es *EmailSender) SendResetPasswordEmail(to, code string) error {
	msg := fmt.Sprintf(resetPasswordText, to, code)
	log.Println(msg)
	err := smtp.SendMail(es.server, nil, from, []string{to}, []byte(msg))
	if err != nil {
		log.Println(err.Error())
	}
	return err
}
