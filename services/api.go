package services

import (
	"ti_do/data"

	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type ApiClientI interface {
	ExecuteRequest(string, string, string, interface{}) (*http.Response, error)
}

type ApiClient struct {
	address string
	prefix  string
}

func NewApiClient() *ApiClient {
	address := fmt.Sprintf("http://%s:%d", data.ApiConfig.ApiHost, data.ApiConfig.ApiPort)
	return &ApiClient{
		address: address,
		prefix:  "/api",
	}
}

func (ac *ApiClient) ExecuteRequest(method, endpoint, token string, req interface{}) (*http.Response, error) {
	cl := &http.Client{}
	var body io.Reader
	if req != nil {
		j, _ := json.Marshal(&req)
		body = bytes.NewBuffer(j)
	}
	hreq, _ := http.NewRequest(
		method,
		fmt.Sprintf("%s%s%s", ac.address, ac.prefix, endpoint), body)
	if token != "" {
		hreq.Header.Set("X-Api-Token", token)
	}
	res, err := cl.Do(hreq)
	return res, err
}
