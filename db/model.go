package db

import (
	"database/sql"
	"time"
)

type User struct {
	Id                    int64          `sql:"id"`
	Uuid                  string         `sql:"uuid"`
	Email                 string         `sql:"email"`
	Password              string         `sql:"password"`
	GoogleAccount         bool           `sql:"google_account"`
	EmailVerified         bool           `sql:"email_verified"`
	EmailVerificationCode sql.NullString `sql:"email_verification_code"`
	ResetPasswordCode     sql.NullString `sql:"reset_password_code"`
	CreatedAt             []uint8        `sql:"created_at"`
	UpdatedAt             []uint8        `sql:"updated_at"`
}

type UserProfile struct {
	Id        int64          `sql:"id"`
	UserId    int64          `sql:"user_id"`
	UserUuid  string         `sql:"user_uuid"`
	Phone     sql.NullString `sql:"phone"`
	Address   sql.NullString `sql:"address"`
	FullName  sql.NullString `sql:"full_name"`
	CreatedAt time.Time      `sql:"created_at"`
	UpdatedAt time.Time      `sql:"updated_at"`
}

type Authentication struct {
	Id                 int64          `sql:"id"`
	UserId             int64          `sql:"user_id"`
	GoogleAccessToken  sql.NullString `sql:"google_access_token"`
	GoogleRefreshToken sql.NullString `sql:"google_refresh_token"`
	ApiAccessToken     sql.NullString `sql:"api_access_token"`
	ApiTokenExpiresAt  time.Time      `sql:"api_token_expires_at"`
	CreatedAt          time.Time      `sql:"created_at"`
	UpdatedAt          time.Time      `sql:"updated_at"`
}
