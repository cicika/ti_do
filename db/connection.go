package db

import (
	"ti_do/data"

	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

type MysqlConnectionI interface {
	InsertUser(*User) (int64, error)
	UpdatePassword(string, string) error
	UpdateEmail(int64, string) error
	SelectUser(int64, string, string) (*User, *UserProfile, string, error)
	InsertUserProfile(*UserProfile) (int64, error)
	UpdateUserProfile(*UserProfile) (int64, error)
	InsertApiAccessToken(int64, string) error
	UpdateApiAccessToken(int64, string) error
	Login(string) (*User, error)
	InvalidateAccessToken(int64) error
	SelectApiAccessToken(string) (int64, error)
	CheckEmailVerificationCode(string) (string, error)
	VerifyEmail(string) (int64, error)
	InsertPasswordCode(string, string) (int64, error)
}

type MysqlConn struct {
	db *sql.DB
}

func NewMysqlConn() (*MysqlConn, error) {
	return initDb()
}

func (ms *MysqlConn) InsertUser(user *User) (int64, error) {
	row, err := insertUser.Exec(
		user.Uuid, user.Email, user.Password,
		user.GoogleAccount, user.EmailVerified, user.EmailVerificationCode)
	if err != nil {
		return -1, err
	}
	return row.LastInsertId()
}

func (ms *MysqlConn) UpdatePassword(password, code string) error {
	_, err := updatePassword.Exec(password, code)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	row := idForCode.QueryRow(code)

	var id int64
	err = row.Scan(&id)
	if err != nil {

		log.Println(err.Error())
		return err
	}
	_, err = deletePasswordCode.Exec(id)
	return err
}

func (ms *MysqlConn) InsertPasswordCode(email, code string) (int64, error) {
	row, err := insertPasswordCode.Exec(code, email)
	if err != nil {
		return -1, err
	}
	return row.RowsAffected()
}

func (ms *MysqlConn) UpdateEmail(userId int64, email string) error {
	_, err := updateEmail.Exec(email, userId)

	return err
}

func (ms *MysqlConn) SelectUser(userId int64, userUuid string, email string) (*User, *UserProfile, string, error) {
	row := selectUser.QueryRow(userId, userUuid, email)
	var u User
	var uP UserProfile
	var token string

	err := row.Scan(&u.Id, &u.Uuid, &u.Email, &u.GoogleAccount,
		&u.EmailVerified, &uP.FullName, &uP.Address, &uP.Phone,
		&token)

	return &u, &uP, token, err
}

func (ms *MysqlConn) InsertUserProfile(profile *UserProfile) (int64, error) {
	row, err := insertUserProfile.Exec(
		profile.UserId, profile.UserUuid, profile.Phone,
		profile.Address, profile.FullName,
	)
	if err != nil {
		return -1, err
	}

	return row.LastInsertId()
}

func (ms *MysqlConn) UpdateUserProfile(profile *UserProfile) (int64, error) {
	values := updateProfileStmt(profile)
	stmt := prepareStatement(updateUserProfile, ms.db)
	lv := len(values)
	var row sql.Result
	var err error
	if lv == 0 {
		return 0, nil
	}
	if lv == 1 {
		row, err = stmt.Exec(values[0], profile.UserId)
	}
	if lv == 2 {
		row, err = stmt.Exec(values[0], values[1], profile.UserId)
	}
	if lv == 3 {
		row, err = stmt.Exec(values[0], values[1], values[2], profile.UserId)
	}
	if err != nil {
		log.Println(err.Error())
		return -1, nil
	}

	return row.RowsAffected()
}

func (ms *MysqlConn) InsertApiAccessToken(userId int64, token string) error {
	_, err := insertBasicAuth.Exec(userId, token)

	return err
}

func (ms *MysqlConn) UpdateApiAccessToken(userId int64, token string) error {
	_, err := updateAuthWithApiToken.Exec(token, userId)

	return err
}

func (ms *MysqlConn) Login(email string) (*User, error) {
	row := loginQuery.QueryRow(email)
	var u User

	err := row.Scan(&u.Id, &u.Uuid, &u.Email, &u.Password,
		&u.GoogleAccount, &u.EmailVerified, &u.EmailVerificationCode,
		&u.ResetPasswordCode, &u.CreatedAt, &u.UpdatedAt,
	)

	return &u, err
}

func (ms *MysqlConn) InvalidateAccessToken(userId int64) error {
	_, err := logoutQuery.Exec(userId)
	if err != nil {
		log.Println(err.Error())
	}
	return err
}

func (ms *MysqlConn) CheckEmailVerificationCode(code string) (string, error) {
	row := checkEmailValidationCode.QueryRow(code)

	var email string
	err := row.Scan(&email)
	return email, err
}

func (ms *MysqlConn) VerifyEmail(email string) (int64, error) {
	row, err := validateEmail.Exec(email)
	if err != nil {
		return 0, err
	}
	return row.RowsAffected()
}

func (ms *MysqlConn) SelectApiAccessToken(token string) (int64, error) {
	row := selectUserApiToken.QueryRow(token)
	var userId int64

	err := row.Scan(&userId)
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return userId, nil
}

func initDb() (*MysqlConn, error) {
	dbInfo := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s",
		data.ApiConfig.DatabaseUsername,
		data.ApiConfig.DatabasePassword,
		data.ApiConfig.DatabaseHost,
		data.ApiConfig.DatabasePort,
		data.ApiConfig.DatabaseName,
	)

	Db, err := sql.Open("mysql", dbInfo)
	if err != nil {
		return &MysqlConn{}, err
	}
	Db.SetConnMaxLifetime(0)
	Db.SetMaxIdleConns(50)
	Db.SetMaxOpenConns(50)

	prepareStatements(Db)
	return &MysqlConn{db: Db}, nil
}
