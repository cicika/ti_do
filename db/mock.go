package db

import (
	"github.com/stretchr/testify/mock"
)

type MySqlConnMock struct {
	mock.Mock
}

func (db *MySqlConnMock) InsertUser(user *User) (int64, error) {
	args := db.Called(user)
	if args != nil {
		if val, ok := args.Get(0).(int64); ok {
			return val, args.Error(1)
		}
	}
	return -1, args.Error(1)
}

func (db *MySqlConnMock) UpdatePassword(password, code string) error {
	args := db.Called(password, code)
	if args != nil {
		return args.Error(0)
	}

	return nil
}
func (db *MySqlConnMock) InsertPasswordCode(email, code string) (int64, error) {
	args := db.Called(email, code)
	if args != nil {
		if val, ok := args.Get(0).(int64); ok {
			return val, args.Error(1)
		}
	}
	return -1, args.Error(1)
}
func (db *MySqlConnMock) UpdateEmail(userId int64, email string) error {
	args := db.Called(userId, email)
	if args != nil {
		return args.Error(0)
	}

	return nil
}

func (db *MySqlConnMock) SelectUser(userId int64, uuid, email string) (*User, *UserProfile, string, error) {
	args := db.Called(userId, uuid, email)
	if args != nil {
		u, uOk := args.Get(0).(*User)
		up, upOk := args.Get(1).(*UserProfile)
		t, tOk := args.Get(2).(string)
		if uOk && upOk && tOk {
			return u, up, t, nil
		}
	}
	return &User{}, &UserProfile{}, "", args.Error(3)
}

func (db *MySqlConnMock) InsertUserProfile(up *UserProfile) (int64, error) {
	args := db.Called(up)
	if args != nil {
		if val, ok := args.Get(0).(int64); ok {
			return val, args.Error(1)
		}
	}
	return -1, args.Error(1)
}

func (db *MySqlConnMock) UpdateUserProfile(up *UserProfile) (int64, error) {
	args := db.Called(up)
	if args != nil {
		if val, ok := args.Get(0).(int64); ok {
			return val, args.Error(1)
		}
	}
	return -1, args.Error(1)
}

func (db *MySqlConnMock) InsertApiAccessToken(userId int64, token string) error {
	args := db.Called(userId, token)
	if args != nil {
		return args.Error(0)
	}
	return nil
}

func (db *MySqlConnMock) UpdateApiAccessToken(userId int64, token string) error {
	args := db.Called(userId, token)
	if args != nil {
		return args.Error(0)
	}
	return nil
}

func (db *MySqlConnMock) Login(username string) (*User, error) {
	args := db.Called(username)
	if args != nil {
		if val, ok := args.Get(0).(*User); ok {
			return val, args.Error(1)
		}
	}
	return &User{}, args.Error(1)
}

func (db *MySqlConnMock) InvalidateAccessToken(userId int64) error {
	args := db.Called(userId)
	if args != nil {
		return args.Error(0)
	}
	return nil
}

func (db *MySqlConnMock) SelectApiAccessToken(token string) (int64, error) {
	args := db.Called(token)
	if args != nil {
		if val, ok := args.Get(0).(int64); ok {
			return val, args.Error(1)
		}
	}
	return -1, args.Error(1)
}

func (db *MySqlConnMock) CheckEmailVerificationCode(code string) (string, error) {
	args := db.Called(code)
	if args != nil {
		if val, ok := args.Get(0).(string); ok {
			return val, args.Error(1)
		}
	}
	return "", args.Error(1)
}

func (db *MySqlConnMock) VerifyEmail(email string) (int64, error) {
	args := db.Called(email)
	if args != nil {
		if val, ok := args.Get(0).(int64); ok {
			return val, args.Error(1)
		}
	}
	return -1, args.Error(1)
}
