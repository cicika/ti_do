package db

import (
	"database/sql"
	"fmt"
	"log"
)

var insertUser, updatePassword, updateEmail, insertUserProfile *sql.Stmt
var selectUser, loginQuery, logoutQuery, insertPasswordCode *sql.Stmt
var insertBasicAuth, updateAuthWithApiToken, selectUserApiToken *sql.Stmt
var validateEmail, checkEmailValidationCode, deletePasswordCode, idForCode *sql.Stmt

var updateUserProfile string

func prepareUserQueries(db *sql.DB) {
	insertUserQ := "INSERT INTO users " +
		"(uuid, email, password, google_account, email_verified, " +
		"email_verification_code, created_at, updated_at) " +
		"VALUES (?, ?, ?, ?, ?, ?, now(), now())"
	updatePasswordQ := "UPDATE users SET password = ? " +
		"WHERE reset_password_code = ?"
	idForCodeQ := "SELECT id FROM users WHERE reset_password_code = ?"
	deletePasswordCodeQ := "UPDATE users SET reset_password_code = '' WHERE id = ?"
	updateEmailQ := "UPDATE users SET email = ? WHERE id = ?"
	insertUserProfileQ := "INSERT INTO user_profiles " +
		"(user_id, user_uuid, phone, address, full_name, created_at, updated_at) " +
		"VALUES (?, ?, ?, ?, ?, now(), now())"
	selectUserQ := "SELECT u.id, u.uuid, u.email, u.google_account, " +
		"u.email_verified, p.full_name, p.address, p.phone, a.api_access_token " +
		"FROM users AS u " +
		"LEFT JOIN user_profiles AS p ON u.id = p.user_id " +
		"LEFT JOIN authentication AS a ON u.id = a.user_id " +
		"WHERE u.id = ? OR u.uuid = ? OR email = ?"
	insertPasswordCodeQ := "UPDATE users SET reset_password_code = ? WHERE email = ?"

	insertUser = prepareStatement(insertUserQ, db)
	updatePassword = prepareStatement(updatePasswordQ, db)
	updateEmail = prepareStatement(updateEmailQ, db)
	insertUserProfile = prepareStatement(insertUserProfileQ, db)
	selectUser = prepareStatement(selectUserQ, db)
	insertPasswordCode = prepareStatement(insertPasswordCodeQ, db)
	deletePasswordCode = prepareStatement(deletePasswordCodeQ, db)
	idForCode = prepareStatement(idForCodeQ, db)
}

func prepareAuthQueries(db *sql.DB) {
	insertBasicAuthQ := "INSERT INTO authentication (user_id, api_access_token, " +
		"api_token_expires_at, created_at, updated_at) " +
		"VALUES (?, ?, DATE_ADD(now(), INTERVAL 2 HOUR), now(), now())"
	updateAuthWithApiTokenQ := "UPDATE authentication SET api_access_token = ?, " +
		"api_token_expires_at = DATE_ADD(now(), INTERVAL 2 HOUR), updated_at = now() WHERE user_id = ?"
	loginQueryQ := "SELECT * FROM users WHERE email = ?"
	logoutQueryQ := "UPDATE authentication SET api_access_token = '', " +
		"api_token_expires_at = now(), updated_at = now() " +
		"WHERE user_id = ?"
	selectUserApiTokenQ := "SELECT user_id FROM authentication " +
		"WHERE api_access_token = ? AND api_token_expires_at > now()"

	insertBasicAuth = prepareStatement(insertBasicAuthQ, db)
	updateAuthWithApiToken = prepareStatement(updateAuthWithApiTokenQ, db)
	loginQuery = prepareStatement(loginQueryQ, db)
	logoutQuery = prepareStatement(logoutQueryQ, db)
	selectUserApiToken = prepareStatement(selectUserApiTokenQ, db)
}

func prepareEmailValidationQueries(db *sql.DB) {
	checkEmailValidationCodeQ := "SELECT email FROM users WHERE " +
		"email_verification_code = ?"
	validateEmailQ := "UPDATE users SET email_verified = true, " +
		"email_verification_code = NULL WHERE email = ?"

	checkEmailValidationCode = prepareStatement(checkEmailValidationCodeQ, db)
	validateEmail = prepareStatement(validateEmailQ, db)
}

func updateProfileStmt(up *UserProfile) []sql.NullString {
	baseQ := "UPDATE user_profiles SET "
	where := " WHERE user_id = ?"
	var parts []string
	var elem []sql.NullString
	if up.FullName.Valid {
		parts = append(parts, "full_name = ?")
		elem = append(elem, up.FullName)
	}
	if up.Address.Valid {
		parts = append(parts, "address = ?")
		elem = append(elem, up.Address)
	}
	if up.Phone.Valid {
		parts = append(parts, "phone = ?")
		elem = append(elem, up.Phone)
	}
	l := len(parts)
	var q string
	for i, p := range parts {
		q += p
		if i < l-1 {
			q += ", "
		}
	}
	updateUserProfile = fmt.Sprintf("%s%s%s", baseQ, q, where)
	return elem
}

func prepareStatement(sql string, db *sql.DB) *sql.Stmt {
	stmt, err := db.Prepare(sql)
	if err != nil {
		log.Println(sql)
		log.Println(err)
	}
	return stmt
}

func prepareStatements(db *sql.DB) {
	prepareAuthQueries(db)
	prepareUserQueries(db)
	prepareEmailValidationQueries(db)
}
