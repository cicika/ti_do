package data

var ApiConfig *ApiConfiguration
var FrontConfig *FrontendConfiguration

type ApiConfiguration struct {
	ApiHost          string `json:"api_host"`
	ApiPort          int    `json:"api_port"`
	DatabaseHost     string `json:"db_host"`
	DatabasePort     int    `json:"db_port"`
	DatabaseUsername string `json:"db_user"`
	DatabasePassword string `json:"db_password"`
	DatabaseName     string `json:"db_name"`
	EmailServer      string `json:"email_server"`
}

type FrontendConfiguration struct {
	ApiHost      string `json:"api_host"`
	ApiPort      int    `json:"api_port"`
	FrontendHost string `json:"front_host"`
	FrontendPort int    `json:"front_port"`
}
