package main

import (
	"ti_do/api"
	"ti_do/data"
	"ti_do/db"
	"ti_do/front"

	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	loadConfig()
	mysql, err := db.NewMysqlConn()
	if err != nil {
		log.Fatal("Error initializing Database connection: ", err.Error())
	}
	f, err := os.OpenFile("app.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetOutput(f)

	go api.NewApiServer(mysql)
	front.NewFrontendServer()
}

func loadConfig() {
	f, err := ioutil.ReadFile("./resources/apiConfig.json")
	if err != nil {
		log.Fatal("Could not read API config file!")
	}
	err = json.Unmarshal(f, &data.ApiConfig)
	if err != nil {
		log.Fatal("Error unmarshalling API config: ", err.Error())
	}

	f, err = ioutil.ReadFile("./resources/frontConfig.json")
	if err != nil {
		log.Fatal("Could not read Frontend config file!")
	}
	err = json.Unmarshal(f, &data.FrontConfig)
	if err != nil {
		log.Fatal("Error unmarshalling Frontend config: ", err.Error())
	}
}
