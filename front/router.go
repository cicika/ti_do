package front

import (
	"ti_do/api"
	"ti_do/data"
	svc "ti_do/services"

	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

const (
	headerTpl         = "./tpl/header.tpl"
	footerTpl         = "./tpl/footer.tpl"
	userMessageTpl    = "./tpl/user-message.tpl"
	registrationTpl   = "./tpl/registration.tpl"
	loginTpl          = "./tpl/login.tpl"
	profileTpl        = "./tpl/profile.tpl"
	editProfileTpl    = "./tpl/edit-profile.tpl"
	forgotPasswordTpl = "./tpl/forgot-password.tpl"
	resetPasswordTpl  = "./tpl/reset-password.tpl"
)

type TemplateData struct {
	PageTitle string
	Error     bool
	Info      bool
	Message   string
	User      api.FullUserProfile
}

type FrontendServer struct {
	address  string
	includes []string
	api      svc.ApiClientI
}

func NewFrontendServer() {
	adr := fmt.Sprintf(
		"%s:%d",
		data.FrontConfig.FrontendHost, data.FrontConfig.FrontendPort)
	includes := []string{headerTpl, footerTpl, userMessageTpl}
	fs := &FrontendServer{
		address:  adr,
		includes: includes,
		api:      svc.NewApiClient(),
	}

	fs.startFrontendServer()
}

func (fs *FrontendServer) startFrontendServer() {
	router := mux.NewRouter().StrictSlash(true)

	fileServer := http.FileServer(http.Dir("./assets/"))
	router.PathPrefix("/fe/").Handler(http.StripPrefix("/fe/", fileServer))

	router.Use(fs.cacheControl)
	router.Methods("GET").Path("/").HandlerFunc(fs.Index)
	router.Methods("POST").Path("/login").HandlerFunc(fs.Login)
	router.Methods("POST").Path("/logout").HandlerFunc(fs.Logout)
	router.Methods("GET").Path("/profile").HandlerFunc(fs.ProfilePage)
	router.Methods("POST").Path("/profile").HandlerFunc(fs.Profile)
	router.Methods("GET").Path("/forgot-password").HandlerFunc(fs.ForgotPasswordForm)
	router.Methods("POST").Path("/forgot-password").HandlerFunc(fs.ForgotPassword)
	router.Methods("GET").Path("/registration").HandlerFunc(fs.RegistrationPage)
	router.Methods("POST").Path("/registration").HandlerFunc(fs.Registration)
	router.Methods("POST").Path("/google-signup").HandlerFunc(fs.GoogleSignup)
	router.Methods("GET").Path("/verify-email/{code}").HandlerFunc(fs.VerifyEmail)
	router.Methods("GET").Path("/reset-password/{code}").HandlerFunc(fs.ResetPasswordForm)
	router.Methods("POST").Path("/reset-password").HandlerFunc(fs.ResetPassword)
	router.Methods("GET").Path("/setup-profile").HandlerFunc(fs.EditProfile)
	router.Methods("GET").Path("/edit-profile").HandlerFunc(fs.EditProfile)

	log.Fatal(http.ListenAndServe(fs.address, router))
}
