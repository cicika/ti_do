package front

import (
	"ti_do/api"

	"encoding/json"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/gorilla/securecookie"
)

var decoder = schema.NewDecoder()
var hashKey = []byte("6Hnlkdks8gllOp40Dfm_sdf8")
var blockKey = []byte("Kl82Hgfp94eRfm4eAcgbUj3lKo32jmGh")
var s = securecookie.New(hashKey, blockKey)

func (fs *FrontendServer) Index(w http.ResponseWriter, r *http.Request) {
	setCookie(w, api.FullUserProfile{}, true)
	tplData := TemplateData{PageTitle: "Login"}
	fs.render(w, "login", loginTpl, tplData)
}

func (fs *FrontendServer) ForgotPasswordForm(w http.ResponseWriter, r *http.Request) {
	tplData := TemplateData{PageTitle: "Forgot password"}
	fs.render(w, "forgot-password", forgotPasswordTpl, tplData)
}

func (fs *FrontendServer) ForgotPassword(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Println("Error parsing form: ", err.Error())
	}
	var data api.LoginRegister
	decoder.Decode(&data, r.PostForm)
	res, err := fs.api.ExecuteRequest("POST", "/forgot-password", "", data)

	var errr bool = false
	var info bool = true
	var msg string = "A link for resetting your password has been sent to the supplied email"

	if err != nil || res.StatusCode == http.StatusInternalServerError {
		errr = true
		info = false
		msg = "Something went wrong. Please try again later"
	}

	if res.StatusCode == http.StatusNotFound {
		info = true
		msg = "Supplied email does not exist in our database"
	}
	tplData := TemplateData{
		PageTitle: "Forgot password",
		Error:     errr,
		Info:      info,
		Message:   msg,
	}
	fs.render(w, "forgot-password", forgotPasswordTpl, tplData)
}

func (fs *FrontendServer) RegistrationPage(w http.ResponseWriter, r *http.Request) {
	tplData := TemplateData{PageTitle: "Registration"}
	fs.render(w, "registration", registrationTpl, tplData)
}

func (fs *FrontendServer) Registration(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Println("Error parsing form: ", err.Error())
	}
	var data api.LoginRegister
	decoder.Decode(&data, r.PostForm)

	res, err := fs.api.ExecuteRequest("POST", "/registration", "", data)
	log.Println(res.StatusCode)
	if err != nil || (res.StatusCode != http.StatusOK && res.StatusCode != http.StatusConflict) {
		tplData := TemplateData{
			PageTitle: "Registration",
			Error:     true,
			Message:   "Something went wrong. Please try again later.",
		}
		fs.render(w, "registration", registrationTpl, tplData)
		return
	}

	if res.StatusCode == http.StatusConflict {
		tplData := TemplateData{
			Error:   true,
			Message: "User with submitted email already exists!",
		}
		fs.render(w, "registration", registrationTpl, tplData)
		return
	}

	up := userProfile(res)
	setCookie(w, up, false)
	http.Redirect(w, r, "/setup-profile", 302)
}

func (fs *FrontendServer) GoogleSignup(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Println("Error parsing form: ", err.Error())
	}
	var g api.GoogleSignup
	decoder.Decode(&g, r.PostForm)
	res, err := fs.api.ExecuteRequest("POST", "/google-signup", "", g)

	if err != nil || res.StatusCode == http.StatusInternalServerError {
		log.Println(err.Error())
		tplData := TemplateData{
			PageTitle: "Registration",
			Error:     true,
			Message:   "There was an error communicating with Google API",
		}
		fs.render(w, "registration", registrationTpl, tplData)
	}
	up := userProfile(res)
	setCookie(w, up, false)
	if up.Existing {
		http.Redirect(w, r, "/profile", 302)
		return
	}
	http.Redirect(w, r, "/setup-profile", 302)
}

func (fs *FrontendServer) EditProfile(w http.ResponseWriter, r *http.Request) {
	cookieVals := cookieValues(r)
	apiKey, ok := cookieVals["key"]
	if !ok || apiKey == "" {
		http.Redirect(w, r, "/", 302)
		return
	}
	res, err := fs.api.ExecuteRequest("GET", "/user/profile", apiKey, nil)
	if err != nil {
		log.Println(err.Error())
	}
	tplData := TemplateData{
		PageTitle: "Welcome to your profile",
		Info:      true,
		Message:   "Here you can complete your profile",
		User:      userProfile(res),
	}
	fs.render(w, "edit-profile", editProfileTpl, tplData)
}

func (fs *FrontendServer) VerifyEmail(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	code, _ := vars["code"]

	res, err := fs.api.ExecuteRequest("GET", "/verify-email/"+code, "", nil)
	var errr bool
	var info bool
	var msg string

	if err != nil || res.StatusCode != http.StatusOK {
		errr = true
		msg = "There was an error verifying your email"
	} else {
		info = true
		msg = "Your email was successfully validated, please log in again"
	}

	tplData := TemplateData{
		PageTitle: "Login",
		Error:     errr,
		Info:      info,
		Message:   msg,
	}
	fs.render(w, "login", loginTpl, tplData)
}

func (fs *FrontendServer) ResetPasswordForm(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	code, _ := vars["code"]

	tplData := TemplateData{
		PageTitle: "Reset password",
		User:      api.FullUserProfile{ResetPasswordCode: code},
	}
	fs.render(w, "reset-password", resetPasswordTpl, tplData)
}

func (fs *FrontendServer) ResetPassword(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Println("Error parsing form: ", err.Error())
	}

	if r.PostForm["password"][0] != r.PostForm["password-again"][0] {
		tplData := TemplateData{
			PageTitle: "Reset password",
			Error:     true,
			Message:   "Passwords don't match!",
			User:      api.FullUserProfile{ResetPasswordCode: r.PostForm["code"][0]},
		}
		fs.render(w, "reset-password", resetPasswordTpl, tplData)
		return
	}

	var data api.LoginRegister
	decoder.Decode(&data, r.PostForm)
	res, err := fs.api.ExecuteRequest("POST", "/reset-password", "", data)

	var tplData TemplateData
	if err != nil || res.StatusCode != http.StatusOK {
		tplData.PageTitle = "Login"
		tplData.Error = true
		tplData.Message = "Something went wrong, try again or request a new password reset."
	}

	tplData.PageTitle = "Login"
	tplData.Info = true
	tplData.Message = "You can now login with your new credentials"

	fs.render(w, "login", loginTpl, tplData)
}

func (fs *FrontendServer) Login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	var data api.LoginRegister
	decoder.Decode(&data, r.PostForm)
	res, err := fs.api.ExecuteRequest("POST", "/login", "", data)

	var tplData TemplateData
	if err != nil || res.StatusCode == http.StatusInternalServerError {
		tplData.PageTitle = "Login"
		tplData.Error = true
		tplData.Message = "Something went wrong. Please try again later"
		fs.render(w, "login", loginTpl, tplData)
		return
	}

	if res.StatusCode == http.StatusNotFound {
		tplData.PageTitle = "Login"
		tplData.Error = true
		tplData.Message = "User with requested email does not exist"
		fs.render(w, "login", loginTpl, tplData)
		return
	}

	up := userProfile(res)
	setCookie(w, up, false)
	http.Redirect(w, r, "/profile", 302)
}

func (fs *FrontendServer) Logout(w http.ResponseWriter, r *http.Request) {
	cookieVals := cookieValues(r)
	apiKey, ok := cookieVals["key"]
	if !ok || apiKey == "" {
		setCookie(w, api.FullUserProfile{}, true)
		http.Redirect(w, r, "/", 302)
	}
	res, err := fs.api.ExecuteRequest("GET", "/logout", apiKey, nil)
	if err != nil {
		log.Println(err.Error())
	} else {
		log.Println("StatusCode ", res.StatusCode)
	}
	setCookie(w, api.FullUserProfile{}, true)
	http.Redirect(w, r, "/", 302)
}

func (fs *FrontendServer) ProfilePage(w http.ResponseWriter, r *http.Request) {
	cookieVals := cookieValues(r)
	apiKey, ok := cookieVals["key"]
	if !ok || apiKey == "" {
		setCookie(w, api.FullUserProfile{}, true)
		http.Redirect(w, r, "/", 302)
		return
	}
	res, err := fs.api.ExecuteRequest("GET", "/user/profile", apiKey, nil)
	if err != nil {
		log.Println(err.Error())
	}
	if res.StatusCode == http.StatusForbidden {
		setCookie(w, api.FullUserProfile{}, true)
		http.Redirect(w, r, "/", 302)
		return
	}
	var tplData TemplateData
	tplData.PageTitle = "Profile"
	if err != nil || res.StatusCode == http.StatusInternalServerError {
		tplData.Error = true
		tplData.Message = "Couldn't fetch profile data"
	}
	up := userProfile(res)
	tplData.User = up

	fs.render(w, "profile", profileTpl, tplData)
}

func (fs *FrontendServer) Profile(w http.ResponseWriter, r *http.Request) {
	cookieVals := cookieValues(r)
	apiKey, ok := cookieVals["key"]
	if !ok || apiKey == "" {
		setCookie(w, api.FullUserProfile{}, true)
		http.Redirect(w, r, "/", 302)
		return
	}
	r.ParseForm()
	var data api.UserProfile
	decoder.Decode(&data, r.PostForm)

	res, err := fs.api.ExecuteRequest("PUT", "/user/profile", apiKey, data)
	if err != nil {
		log.Println(err.Error())
	}

	if res.StatusCode == http.StatusForbidden {
		http.Redirect(w, r, "/", 302)
		return
	}

	var tplData TemplateData
	tplData.PageTitle = "Profile"
	if err != nil || res.StatusCode == http.StatusInternalServerError {
		tplData.Error = true
		tplData.Message = "Couldn't fetch profile data"
	}
	up := userProfile(res)
	tplData.User = up

	fs.render(w, "profile", profileTpl, tplData)
}

func (fs *FrontendServer) render(w http.ResponseWriter, name string, file string, data TemplateData) {
	tpl := template.New(name)
	tpl, _ = tpl.ParseFiles(append(fs.includes, file)...)
	tpl.Execute(w, data)
}

func setCookie(w http.ResponseWriter, up api.FullUserProfile, empty bool) {
	cookie := new(http.Cookie)
	value := make(map[string]string)
	if empty {
		encoded, _ := s.Encode("test-cookie", value)
		cookie.Name = "test-cookie"
		cookie.Value = encoded
		cookie.Path = "/"
		cookie.Secure = false
		cookie.HttpOnly = true
		cookie.MaxAge = -1
	} else {
		value["key"] = up.AccessToken
		value["uuid"] = up.Uuid
		encoded, _ := s.Encode("test-cookie", value)
		cookie.Name = "test-cookie"
		cookie.Value = encoded
		cookie.Path = "/"
		cookie.Secure = false
		cookie.HttpOnly = true
		cookie.MaxAge = 2 * 60 * 60
	}

	http.SetCookie(w, cookie)
}

func cookieValues(r *http.Request) map[string]string {
	cookie, err := r.Cookie("test-cookie")
	value := make(map[string]string)

	if err != nil {
		log.Println(err)
		return value
	}
	err = s.Decode("test-cookie", cookie.Value, &value)
	if err != nil {
		log.Println(err.Error())
	}
	return value
}

func userProfile(res *http.Response) api.FullUserProfile {
	body, _ := ioutil.ReadAll(res.Body)
	var up api.FullUserProfile
	json.Unmarshal(body, &up)
	return up
}
