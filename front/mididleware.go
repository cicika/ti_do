package front

import (
  "net/http"
)

func (as *FrontendServer) cacheControl(next http.Handler) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Cache-control", "no-cache")
      next.ServeHTTP(w, r)
    })
}
